---
marp: true

theme: default
header: ''
footer: ''
backgroundImage: url('./styles/background.png')
title: Cooperation Event Saxony-Anhalt and european partner regions
author: Christian Willberg
---



# Cooperation Event: Saxony-Anhalt and European partner regions
Prof. Dr.-Ing.  Christian Willberg<a href="https://orcid.org/0000-0003-2433-9183"><img src="styles/ORCIDiD_iconvector.png" alt="ORCID Symbol" style="height:15px;width:auto;vertical-align: top;background-color:transparent;"></a>
Magdeburg-Stendal University of Applied Science


Contact: christian.willberg@h2.de

![bg right 60% fit](./styles/qr-code.png)

---

## Composite materials and bio composites 

- analysis and experimental characterisation
- test can be cyclic, mixed mode and temperature loaded 
(-100°C - +200°C)

![bg right fit](./Figures/composites.png)

---


## High perfomance fracture analysis 
- mechanical
- frature
- thermal
- corrosion
- H2 permeation
- additive manufacturing

![bg right fit](https://raw.githubusercontent.com/PeriHub/PeriLab.jl/main/assets/PeriLab_crack.gif)
![bg vertical fit](https://raw.githubusercontent.com/PeriHub/PeriLab.jl/main/assets/PeriLab_additive.gif)

---

<iframe src="https://perilab-results.nimbus-extern.dlr.de/models/ForgedCT?step=65&variable=von%20Mises%20Stress&displFactor=20" width="920" height="600"></iframe>


<div style="position: absolute; bottom: 70px; left: 1000px; color: blue; font-size: 20px;"> 
    <img src="./styles/qr-code_models.png" alt="Presentation link" style="height:250px;width:auto;vertical-align: top;background-color:transparent;">
</div>

--- 

## Structural health and load monitoring

- simulations of guided waves
- developement of smart sensors
- damage assessment
- network analysis


![bg right fit 60%](./Figures/panel.png)
![bg vertical fit 90%](./Figures/model_plate.png)

---

## Digitalization in material science
- data models
- provenience of data and its acquisition 
- data analysis

![bg right fit 95%](./Figures/digitalisation.png)
